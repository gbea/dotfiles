# dotfiles

Clone the repo and run:

    $ ./deploy.sh


Update vim plugins:

    $ git submodule foreach git pull origin master
