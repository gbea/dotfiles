{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];
  #nix.buildCores = 4;
  #nix.maxJobs = 4;

  boot = {
    loader.grub = {
      enable = true;
      version = 2;
      efiSupport = true;
      device = "/dev/sda";
    };
    initrd.luks.devices = [
      {
        name = "root";
        device = "/dev/sda3";
        preLVM = true;
      }
    ];
  };
  boot.kernel.sysctl."net.ipv4.ip_forward" = 1;

  networking = {
    hostName = "laptop";
    networkmanager.enable = true;
    firewall = {
      enable = true;
      allowedUDPPorts = [ ];
      allowedTCPPorts = [ 8000 ];
    };
  };

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "fr";
    defaultLocale = "en_US.UTF-8";
  };

  time.timeZone = "Europe/Paris";
  environment = { 
    systemPackages = with pkgs; [
      #core packages
      binutils-unwrapped busybox coreutils ctags curl file fzf gnupg htop netcat nmap 
      ntfs3g openvpn p7zip rsync tmux traceroute tree unrar unzip v4l_utils vim wget 
      which xorg.xhost xorg.xkill zip fuse_exfat exfat exfat-utils

      #gnome
      chrome-gnome-shell deepin.deepin-gtk-theme numix-icon-theme-square
      networkmanagerapplet
      gnomeExtensions.battery-status
      gnomeExtensions.topicons-plus
      gnomeExtensions.dash-to-dock

      #user-packages
      atom browserpass chromium gitFull gimp gnome3.cheese google-chrome gparted 
      libreoffice pass seafile-client signal-desktop thunderbird vlc
      gitAndTools.git-annex gitAndTools.gitRemoteGcrypt
    ];

    gnome3.excludePackages = with pkgs.gnome3; [ 
      epiphany evolution totem yelp accerciser gnome-contacts gnome-system-log 
      gnome-music gnome-photos gnome-weather gnome-software gnome-usage 
    ];

    variables.CCNET_CONF_DIR = "/etc/seafile/ccnet";
    interactiveShellInit = ". ${pkgs.gnome3.vte}/etc/profile.d/vte.sh";
  };

  fonts = {
    enableCoreFonts = true;
    fonts = [
      pkgs.mononoki
    ];
  };

  nixpkgs.config = {
    allowUnfree = true;
    chromium.enableGnomeExtensions = true;
  };

  programs = {
    bash.enableCompletion = true;
    browserpass.enable = true;
    chromium = {
      enable = true;
      extensions = [
        "naepdomgkenhinolocfifgehidddafch" # browserpass
        "fhcgjolkccmbidfldomjliifgaodjagh" # cookie autodelete
        "gphhapmejobijbbhgpjhcjognlahblep" # gnome shell integration
        "gcbommkclmclpchllfjekcdonpmejbdp" # https everywhere
        "pkehgijcmpdhfbdbbnkijodmdjhbjlgp" # privacy badger
        "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock origin
      ];
    };
  };

  hardware.bluetooth.enable = false;
  #hardware.pulseaudio = {
  #  enable = true;
  #  package = pkgs.pulseaudioFull;
  #};

  services = {
    xserver = {
      enable = true;
      layout = "fr";
      libinput.enable = true;
      displayManager.gdm.enable = true;
      desktopManager.gnome3.enable = true;
    };

    gnome3 = {
      tracker.enable = false;
      chrome-gnome-shell.enable = true;
    };

    printing = {
      enable = true;
      drivers = [pkgs.hplip];
    };
    teamviewer.enable = true;
  };

  virtualisation.virtualbox.host.enable = true;
  virtualisation.docker.enable = true;

  users.extraUsers.gbea= {
    isNormalUser = true;
    uid = 1000;
    createHome = true;
    home = "/home/gbea";
    extraGroups = ["networkmanager" "users" "wheel" "vboxusers" "docker" ];
  };

  system.nixos.stateVersion = "unstable"; # Did you read the comment?

}
