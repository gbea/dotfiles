#!/usr/bin/env bash


if ! command -v vim &> /dev/null
then
    echo "vim could not be found. Please install it"
    exit
fi

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
dir=$HOME/dotfiles
olddir=$HOME/dotfiles_old
files="bash_profile bashrc vimrc vim inputrc gitconfig tmux.conf nixpkgs config"

if [ "$SCRIPT_DIR" != "$dir" ]; then
    echo "This script should be in $dir, but it is in $SCRIPT_DIR. Please move it"
    exit
fi


# create dotfiles_old in homedir
echo "[INIT] Creating $olddir for backup of any existing dotfiles in $HOME"
mkdir -p $olddir
echo "...done"

# change to the dotfiles directory
echo "[INIT] Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    echo "[BACKUP] Moving any existing $file to $olddir"
    mv $HOME/.$file $HOME/dotfiles_old/
    echo "[SYMLINK] $file"
    ln -s $dir/$file $HOME/.$file
done

ln -s $dir/dircolors/dircolors.ansi-dark $HOME/.dircolors

echo "[VIM] Running :PlugInstall"
vim -c ":PlugInstall | :qa"
