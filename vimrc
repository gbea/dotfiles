call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
Plug 'airblade/vim-gitgutter'
Plug 'docunext/closetag.vim'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ntpeters/vim-better-whitespace'
Plug 'terryma/vim-multiple-cursors'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

"Plug 'scrooloose/syntastic'
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'dense-analysis/ale'
"Plug 'prabirshrestha/vim-lsp' " async lsp support
"Plug 'rhysd/vim-lsp-ale'
"Plug 'mattn/vim-lsp-settings' " lsp auto-configs

" Appearence
Plug 'altercation/vim-colors-solarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Plug 'lifepillar/vim-mucomplete'

Plug 'sheerun/vim-polyglot' " syntax highlighting for all

call plug#end()

filetype indent plugin on
set ruler
set number
set smartindent
set tabstop=4
set shiftwidth=4
set signcolumn=yes
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType html setlocal shiftwidth=2 tabstop=2
autocmd FileType vue setlocal shiftwidth=2 tabstop=2
set expandtab
set cursorline
set cursorcolumn
set undodir=~/.vim/undo
set undofile
set encoding=utf-8
set termencoding=utf-8
set updatetime=250
set background=dark
syntax enable
colorscheme solarized

" Like ctrl D
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_start_word_key      = '<C-d>'
let g:multi_cursor_select_all_word_key = '<A-d>'
let g:multi_cursor_start_key           = 'g<C-d>'
let g:multi_cursor_select_all_key      = 'g<A-d>'
let g:multi_cursor_next_key            = '<C-d>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-x>'
let g:multi_cursor_quit_key            = '<Esc>'

" Like ctrl P
nnoremap <C-p> :Files<Cr>
nnoremap <C-F> :Rg<Cr>

autocmd BufEnter * :syntax sync fromstart
autocmd BufWinEnter * NERDTreeMirror
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" Autocompletion
let b:ale_linters = ['flake8', 'vim-lsp']
let g:ale_linter_aliases = {'vue': ['vue', 'javascript']}
let g:ale_linters = {'vue': ['eslint', 'vls']}
let g:ale_completion_enabled = 1
set omnifunc=ale#completion#OmniFunc
