if [ -e ${HOME}/.nix-profile/etc/profile.d/nix.sh ]; then . ${HOME}/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

shopt -s histappend
HISTTIMEFORMAT="%d/%h - %H:%M:%S "
HISTCONTROL=ignoreboth
HISTSIZE=100000
HISTFILESIZE=200000
HISTIGNORE="ll:ls:la:cd:.."

alias la='ls -A'
alias l='ls -lL'
alias ll='ls -l'

alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

alias c='clear'
alias ..='cd ..'

export PROMPT_DIRTRIM=2
export PS1='\[\e[1;33m\]\t \[\e[0;34m\]\u\[\e[1;0m\]@\[\e[0;33m\]\h\[\e[1;0m\]:\w $(if [ $? != 0 ]; then echo "\[\e[38;5;9m\]"; fi)\$ \[\e[1;0m\]'

EDITOR=vim

eval `dircolors ~/.dircolors`

unset SSH_ASKPASS

if command -v fzf-share >/dev/null; then
  source "$(fzf-share)/key-bindings.bash"
  source "$(fzf-share)/completion.bash"
fi

_fzf_complete_pass() {
  _fzf_complete '+m' "$@" < <(
    pwdir=${PASSWORD_STORE_DIR-~/.password-store/}
    stringsize="${#pwdir}"
    find "$pwdir" -name "*.gpg" -print |
        cut -c "$((stringsize + 1))"-  |
        sed -e 's/\(.*\)\.gpg/\1/'
  )
}
[ -n "$BASH" ] && complete -F _fzf_complete_pass -o default -o bashdefault pass

export FZF_DEFAULT_COMMAND='ag -U --hidden --ignore .git -g ""'
export FZF_DEFAULT_OPTS='--bind "F1:toggle-preview" --preview "rougify {} 2> /dev/null || cat {} 2> /dev/null || tree -C {} 2> /dev/null | head -100" --color dark,hl:33,hl+:37,fg+:235,bg+:136,fg+:254 --color info:254,prompt:37,spinner:108,pointer:235,marker:235'
export FZF_CTRL_R_OPTS='--bind "F1:toggle-preview" --preview "echo {}" --preview-window down:3:hidden:wrap'

export CLICOLOR=1
export ITERM_ENABLE_SHELL_INTEGRATION_WITH_TMUX=YES
